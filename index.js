module.exports = {
    "plugins": [
        "ui5"
    ],
    "extends": [
        "./rules/errors.js",
        "./rules/best_practices.js",
        "./rules/variables.js",
        "./rules/style_issues.js",
        "./rules/es6.js"
    ],
    "env": {
        "browser": true,
        "es6": true
    },
    "globals": {
        "sap": true,
        "module": true
    },
    "rules": {
        "strict": [
            2,
            "function"
        ]
    }

};
