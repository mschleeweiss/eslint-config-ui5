module.exports = {
    rules: {
        "array-bracket-spacing": 2,
        "block-spacing": 2,
        "brace-style": [2, "1tbs", {
            "allowSingleLine": true
        }],
        "camelcase": 2,
        "comma-dangle": [2, "never"],
        "comma-spacing": 2,
        "comma-style": 2,
        "computed-property-spacing": 2,
        "consistent-this": 2,
        "eol-last": 2,
        "func-call-spacing": 2,
        "func-style": 1,
        "key-spacing": 2,
        "keyword-spacing": 2,
        "linebreak-style": 2,
        "new-cap": 2,
        "new-parens": 2,
        "no-array-constructor": 2,
        "no-continue": 2,
        "no-lonely-if": 2,
        "no-mixed-spaces-and-tabs": 2,
        "no-multiple-empty-lines": [2, {
            "max": 2
        }],
        "no-nested-ternary": 2,
        "no-new-object": 2,
        "no-trailing-spaces": 2,
        "no-unneeded-ternary": 2,
        "no-whitespace-before-property": 2,
        "nonblock-statement-body-position": [2, "beside"],
        "one-var": [2, "never"],
        "one-var-declaration-per-line": 2,
        "quotes": 2,
        "semi": 2,
        "semi-spacing": 2,
        "semi-style": 2,
        "space-in-parens": 2,
        "space-infix-ops": 2,
        "switch-colon-spacing": 2,
        "ui5/hungarian-notation": [1, { "onlyDeclarations": true }]
    }
};
