module.exports = {
    rules: {
        "init-declarations": 2,
        "no-delete-var": 2,
        "no-shadow": [2, { "allow": ["oEvent", "resolve", "reject", "done"] }],
        "no-shadow-restricted-names": 2,
        "no-undef": 2,
        "no-undef-init": 2,
        "no-unused-vars": [2, {
            "args": "none"
        }],
        "no-use-before-define": 2
    }
};
