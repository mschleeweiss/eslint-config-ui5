# UI5 Lint Rules

This rule set enforces common best practices in the UI5 context.

It uses most of the standard rules as well as some [custom rules](https://gitlab.com/mschleeweiss/eslint-plugin-ui5/).

## Can I use these rules?

If you target modern browsers but must also support IE11 then yes. The rules enforce ES6 features (`let` and `const`) which are also available in the good old IE11.


## Install
Install the plugin (which contains the custom rules) and the config (which contains the rule set).

    > npm install eslint-plugin-ui5 --save-dev
    > npm install eslint-config-ui5 --save-dev

## Usage

*In your `.eslintrc`*

```json
{
    "extends": "ui5"
}
```
